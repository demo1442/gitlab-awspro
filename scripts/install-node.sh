#!/bin/bash

# Install Node.js and npm
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

# Display Node.js and npm versions
node -v
npm -v
